from django.db                   import models #Traer modelos
from django.contrib.auth.models  import AbstractBaseUser, PermissionsMixin, BaseUserManager #Clases de referencia para la autentificacion-automatizar 
from django.contrib.auth.hashers import make_password

from appLaTienda.models import user #Automatizar cifrado a partir de una palabra clave

class UserManager(BaseUserManager): #self = this //// DAO
    def create_user(self, username, password=None):#Crear usuario
        if not username:
            raise ValueError('El usuario debe tener un username')
        user = self.model(username=username)
        user.set_password(password)
        user.save(using=self._db)#Internamente funciona como slq / ORM
        return user 
    
    def create_superuser(self,username_,password_):#Crear superusuario
        user = self.create_user(
            username = username_,
            password = password_
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    id_user               = models.BigAutoField(primary_key=True)
    username              = models.CharField('Username',max_length=20, unique = True)
    password              = models.CharField('Password',max_length=256)
    first_name            = models.CharField('First_name', max_length=20)
    second_name           = models.CharField('Second_name',max_length=20)
    surname               = models.CharField('Surname',max_length=20)
    second_surname        = models.CharField('Second_surname',max_length=20)
    email                 = models.EmailField('Email', max_length=50)
    identification_number = models.CharField('Identification_number', max_length=15)
    administrator_role    = models.CharField('Administrator_role', max_length=2)
    
    def save(self, **kwargs):
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN' 
        self.password = make_password(self.password, some_salt)
        super().save(**kwargs)

    objects        = UserManager()
    USERNAME_FIELD = 'username'