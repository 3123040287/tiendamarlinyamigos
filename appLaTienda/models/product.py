from django.db import models #Traer modelos
from appLaTienda.models import product

class Product(models.Model):
    id_Pro     = models.AutoField(primary_key=True)
    name       = models.CharField(max_length=50)
    serial     = models.CharField(max_length=50)
    description= models.CharField(max_length=50)
    reference  = models.CharField(max_length=50)
    price      = models.DecimalField(max_digits = 8, decimal_places = 2)