from django.db import models #Traer modelos
from .user     import User
from .product  import Product

class User_Product(models.Model):
    id            = models.AutoField(primary_key=True)
    qualification = models.IntegerField('qualification',default=0)
    user          = models.ForeignKey(User, related_name= 'user_product', on_delete=models.CASCADE) 
    producto      = models.ForeignKey(Product, related_name= 'user_product', on_delete=models.CASCADE)