from django.contrib import admin
from .models.user import User
from .models.product import Product
from .models.user_product import User_Product
admin.site.register(User)
admin.site.register(Product)
admin.site.register(User_Product)
# Register your models here.


